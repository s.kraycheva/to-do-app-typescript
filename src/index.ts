import { v4 as uuidV4 } from 'uuid';

type Task = {
  id: string;
  title: string;
  date: Date;
  completed: boolean;
};

const list = document.querySelector<HTMLUListElement>('#list');
const form = document.getElementById('new-task-form') as HTMLFormElement | null;
const input = document.querySelector<HTMLInputElement>('#new-task-title');
const tasks: Task[] = loadTask();
tasks.forEach(addNewTask);

form?.addEventListener('submit', (e) => {
  e.preventDefault();

  if (input?.value == '' || input?.value == null) return;

  const task: Task = {
    id: uuidV4(),
    title: input.value,
    date: new Date(),
    completed: false,
  };

  tasks.push(task);

  addNewTask(task);
  input.value = '';
});

function addNewTask(task: Task) {
  const item = document.createElement('li');
  const label = document.createElement('label');
  const checkbox = document.createElement('input');
  checkbox.addEventListener('change', () => {
    task.completed = checkbox.checked;
    console.log(tasks);
  });
  checkbox.type = 'checkbox';
  checkbox.checked = task.completed;
  label.append(checkbox, task.title);
  item.append(label);
  list?.append(item);
}

function loadTask(): Task[] {
  const taskJSON = localStorage.getItem('TASKS');

  if (taskJSON == null) return [];
  return JSON.parse(taskJSON);
}
